---
title: "Actualité Avec Humour"
date: 2020-05-10T11:09:57+02:00
description: "Humour"
draft: true
---
Beaucoup d'humoristes savent mettre les points d'incohérences en avant. Cette page a une petite sélection d'artistes que je trouve juste.
## Pierre Emmanuel Barré
Il a un humour cru, sincère et juste.
* [Journal de confinement](https://youtu.be/NW3c5gP09t4)
* [Chroniques sur France Inter avant le confinement](https://youtu.be/dWSH_AsKP-w)
## La Bajon
Humour tout aussi sincère et pertinant.
* [Vidéos dans le désordre](https://youtu.be/KG0Frmom8kM)
## Merri
Il fait des histoires courtes mimant l'actualité.
* [Arrivée du corona virus en France](https://youtu.be/1s9CwKK6ETw)
* [Controle de police pendant le confinement](https://youtu.be/FeyICWnABc0)
## Marcel D.
Youtubeur qui décrit l'actualité avec un oeil critique.
* [Conflits d'intérêt](https://youtu.be/RIA0wKF8Q6Q)
## Dr. Alwest
* [Gestion Covid19](https://youtu.be/eqgVCDkpeVw)
